package LearnAndCode;
import java.util.*;

class HashNode<K, V>{
	private K key;
	private V value;
	private int hashValue;
	
	HashNode(K key, V value){
		this.key = key;
		this.value = value;
		this.hashValue = key.hashCode();
		System.out.println("created hashNode");
	}
	
	public Object getKey() {
		return key;
	}
	
	public Object getHashCodeOfKey(){
		return hashValue;
	}
	
	public Object getValue(){
		return value;
	}
}

class HashBucket<K>{
	private LinkedList<K> linkedList = new LinkedList<K>();
}

 class CustomeHashMap<K, V> {
	private int hashBucketSize;
	private ArrayList<LinkedList<HashNode<K,V>>> hashBucket;
	
	CustomeHashMap(){
		hashBucketSize = 16;
		hashBucket = new ArrayList<LinkedList<HashNode<K,V>>>(hashBucketSize);
		System.out.println("Created Default HashMap");
	}
	
	CustomeHashMap(int defaultSize){
		hashBucketSize = defaultSize;
		hashBucket = new ArrayList<LinkedList<HashNode<K,V>>>(hashBucketSize);
	}
	
	int getHashBucketSize(){
		return hashBucketSize;
	}
	
	public void put(HashNode<K, V> hashNode){
		int hashCode = (int) hashNode.getHashCodeOfKey();
		int index = hashCode & (getHashBucketSize() - 1);
		System.out.println("size : "+hashBucket.size());
		System.out.println("index : "+index);
		System.out.println(hashBucket.get(index));
	
	}
	
	public static void main(String[] args) {
		CustomeHashMap<String, String> customeHashMap = new CustomeHashMap<String, String>();
		HashNode<String, String> hashNode1 = new HashNode<String, String>("devesh", "9928533211");
		customeHashMap.put(hashNode1);
		
	}
}
  

