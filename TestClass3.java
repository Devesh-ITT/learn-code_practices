import java.util.*;
import java.io.*;
public class TestClass3 {
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	void perform(Stack<Integer> stack)throws Exception{
		String s[] = (br.readLine()).split(" ");
		if(s[0].equalsIgnoreCase("p")){
			stack.push(Integer.parseInt(s[1]));
		}else if(s[0].equalsIgnoreCase("b")){
			int x = stack.pop();
			int y = stack.pop();
			stack.push(x);
			stack.push(y);
		}
	}
	
	
	public static void main(String[] args) throws Exception {
		int N,ID;
		TestClass3 fh = new TestClass3();
		int T = Integer.parseInt(fh.br.readLine());
		int output[] = new int[T];
		for(int a =0;a<T;a++){
			Stack<Integer> stack = new Stack<Integer>();
			String str[] = (fh.br.readLine()).split(" ");
			N = Integer.parseInt(str[0]);
			ID = Integer.parseInt(str[1]);
			stack.add(ID);
			for(int b=0;b<N;b++){
				fh.perform(stack);
			}
			output[a] = stack.pop();
		}
		for(int a=0;a<T;a++){
			System.out.println("Player "+output[a]);
		}
	}
}
