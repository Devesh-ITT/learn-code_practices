import org.junit.tests.AllTestsTest.JUnit4Test;
import static org.junit.Assert.*;

import org.junit.Test;

import junit.*;

public class TestCasesForArrayTreeHeight extends JUnit4Test {
	
	@Test
	public void checkRootsCredibility(){
		BST binarySearchTree = new BST();
		Node node = new Node(5);
		binarySearchTree.addNodeInTree(node);
		assertNotNull(binarySearchTree.root);
	}
	
	@Test
	public void checkLeftChildWhenThereISNotAnyChild(){
		BST binarySearchTree = new BST();
		Node firstNode = new Node(5);
		binarySearchTree.addNodeInTree(firstNode);
		Node secondNode = new Node(7);
		binarySearchTree.addNodeInTree(secondNode);
		assertNull(firstNode.leftNode);
	}
	
	@Test
	public void checkRightChildWhenThereISNotAnyChild(){
		BST binarySearchTree = new BST();
		Node firstNode = new Node(5);
		binarySearchTree.addNodeInTree(firstNode);
		Node secondNode = new Node(4);
		binarySearchTree.addNodeInTree(secondNode);
		assertNull(firstNode.rightNode);
	}
	
	@Test
	public void checkHeightForOneChild(){
		BST binarySearchTree = new BST();
		Node node = new Node(5);
		binarySearchTree.addNodeInTree(node);
		assertEquals(1,binarySearchTree.getTreesHeight(binarySearchTree.root));
	}
	
	@Test
	public void checkHeightForRightSkwedTree(){
		BST binarySearchTree = new BST();
		Node node1 = new Node(1);
		binarySearchTree.addNodeInTree(node1);
		Node node2 = new Node(2);
		binarySearchTree.addNodeInTree(node2);
		Node node3 = new Node(3);
		binarySearchTree.addNodeInTree(node3);
		Node node4 = new Node(4);
		binarySearchTree.addNodeInTree(node4);
		Node node5 = new Node(5);
		binarySearchTree.addNodeInTree(node5);
		assertEquals(5, binarySearchTree.getTreesHeight(binarySearchTree.root));
	}
	
	@Test
	public void checkHeightForLeftSkwedTree(){
		BST binarySearchTree = new BST();
		Node node1 = new Node(5);
		binarySearchTree.addNodeInTree(node1);
		Node node2 = new Node(4);
		binarySearchTree.addNodeInTree(node2);
		Node node3 = new Node(3);
		binarySearchTree.addNodeInTree(node3);
		Node node4 = new Node(2);
		binarySearchTree.addNodeInTree(node4);
		Node node5 = new Node(1);
		binarySearchTree.addNodeInTree(node5);
		assertEquals(5, binarySearchTree.getTreesHeight(binarySearchTree.root));
	}
	
	@Test
	public void checkCredibilityOfBSTsHeightHavingChildAtBothPlaces(){
		BST binarySearchTree = new BST();
		Node node1 = new Node(6);
		binarySearchTree.addNodeInTree(node1);
		Node node2 = new Node(4);
		binarySearchTree.addNodeInTree(node2);
		Node node3 = new Node(5);
		binarySearchTree.addNodeInTree(node3);
		Node node4 = new Node(3);
		binarySearchTree.addNodeInTree(node4);
		Node node5 = new Node(9);
		binarySearchTree.addNodeInTree(node5);
		Node node6 = new Node(8);
		binarySearchTree.addNodeInTree(node6);
		Node node7 = new Node(7);
		binarySearchTree.addNodeInTree(node7);
		assertEquals(4, binarySearchTree.getTreesHeight(binarySearchTree.root));
	}
	
	@Test
	public void checkCredibilityOfNodesAtBothPlaces(){
		BST binarySearchTree = new BST();
		Node node1 = new Node(6);
		binarySearchTree.addNodeInTree(node1);
		Node node2 = new Node(4);
		binarySearchTree.addNodeInTree(node2);
		Node node3 = new Node(5);
		binarySearchTree.addNodeInTree(node3);
		Node node4 = new Node(3);
		binarySearchTree.addNodeInTree(node4);
		Node node5 = new Node(9);
		binarySearchTree.addNodeInTree(node5);
		Node node6 = new Node(8);
		binarySearchTree.addNodeInTree(node6);
		Node node7 = new Node(7);
		binarySearchTree.addNodeInTree(node7);
		assertEquals(node2.data, node1.leftNode.data);
		assertEquals(node3.data, node2.rightNode.data);
		assertEquals(node4.data, node2.leftNode.data);
		assertNull(node3.leftNode);
		assertEquals(node5.data, node1.rightNode.data);
		assertNull(node5.rightNode);
		assertEquals(node6.data, node5.leftNode.data);
		assertNull(node6.rightNode);
		assertEquals(node7.data, node6.leftNode.data);
		assertNull(node7.leftNode);
		assertNull(node7.rightNode);
		assertNull(node4.leftNode);
		assertNull(node4.rightNode);
	}
}
