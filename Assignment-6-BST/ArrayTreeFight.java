import java.util.*;

public class ArrayTreeFight {
    Scanner scanner = new Scanner(System.in);
    final int MAX_NUBER_OF_USER_INPUTS = 1000;
    final int MAX_VALUE_OF_EACH_INPUT = 1000000;
    final int MIN_NUBER_OF_USER_INPUTS = 1;
    final int MIN_VALUE_OF_EACH_INPUT = 1;
    BST binarySearchTree = new BST();
    Node convertedTreesRoot;
    int[] arrayOfIntegers;
    boolean areUserInputsValid = true;
    
    public int getNumberOfUserInputs(){
        return scanner.nextInt();
    }
    
    public void getUserInputsForArray(int numberOfInputs){
        arrayOfIntegers = new int[numberOfInputs];
        for(int indexOfArray = 0 ; indexOfArray < numberOfInputs ; indexOfArray++){
            arrayOfIntegers[indexOfArray] = scanner.nextInt();
            if(MAX_VALUE_OF_EACH_INPUT < arrayOfIntegers[indexOfArray] ||  arrayOfIntegers[indexOfArray] < MIN_VALUE_OF_EACH_INPUT){
            	areUserInputsValid = false;
            	return;
            }
        }
    }
    
    public void createTreeFromArray(){
        for (int arrayElement : arrayOfIntegers){
            Node node = new Node(arrayElement);
            binarySearchTree.addNodeInTree(node);
        }
        convertedTreesRoot = binarySearchTree.root;
    }
    
    public int getConvertedTreesHeight(){
        return binarySearchTree.getTreesHeight(convertedTreesRoot);
    }
    
    public static void main(String[] args) throws Exception {
		ArrayTreeFight arrayTreeFight = new ArrayTreeFight();
		int numberOfInputs = arrayTreeFight.getNumberOfUserInputs();
		if(arrayTreeFight.MAX_NUBER_OF_USER_INPUTS < numberOfInputs ||  numberOfInputs < arrayTreeFight.MIN_NUBER_OF_USER_INPUTS){
			return;
		}
		arrayTreeFight.getUserInputsForArray(numberOfInputs);
		if(arrayTreeFight.areUserInputsValid){
			arrayTreeFight.createTreeFromArray();
			System.out.println(arrayTreeFight.getConvertedTreesHeight());
		}else{
			return;
		}
    }

	public demo(){
		hello!	
	}
}
