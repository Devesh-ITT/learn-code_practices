public class BST{
	Node root;
	BST(){
		root = null;
	}
	
	public void addNodeInTree(Node nodeToBeInsert){
		if(root == null){
			root = nodeToBeInsert;
		}else{
			Node temporaryNodeForTraversal= root;
			Node desiredNode = root;
			do{
				if(getDirectionForInsertedNode(temporaryNodeForTraversal, nodeToBeInsert) == "left"){
					desiredNode = temporaryNodeForTraversal;
					temporaryNodeForTraversal = temporaryNodeForTraversal.leftNode;
				}else{
					desiredNode = temporaryNodeForTraversal;
					temporaryNodeForTraversal = temporaryNodeForTraversal.rightNode;
				}
			}while(temporaryNodeForTraversal != null );
			
			if(getDirectionForInsertedNode(desiredNode, nodeToBeInsert) == "left"){
				desiredNode.leftNode = nodeToBeInsert;
			}else{
				desiredNode.rightNode = nodeToBeInsert;
			}
		}
	}
	
	public String getDirectionForInsertedNode(Node baseNode, Node nodeToBeAdded){
		if( nodeToBeAdded.data > baseNode.data) {
			return "right";
		}else{
			return "left";
		}
	}
	
	public int getTreesHeight(Node root){
		if(root != null){
			int leftHeight = 0, rightHeight = 0, height = 0;
			if(root.leftNode != null){
				leftHeight = getTreesHeight(root.leftNode);
			}
			if(root.rightNode != null){
				rightHeight = getTreesHeight(root.rightNode);
			}
			
			if(leftHeight > rightHeight){
				height = leftHeight + 1;
			}else{
				height = rightHeight + 1;
			} 
			return height;
		}else{
			return 0;
		}
	}
}
