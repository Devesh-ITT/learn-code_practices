public class Node{
	public int data;
	public Node leftNode;
	public Node rightNode;
	
	Node(int data){
		this.data = data;
	}
}
