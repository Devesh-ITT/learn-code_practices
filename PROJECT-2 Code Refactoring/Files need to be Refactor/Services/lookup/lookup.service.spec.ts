import { TestBed, inject } from '@angular/core/testing';
import { LookupService } from './lookup.service';
import { HttpClientModule } from '@angular/common/http';
import {HttpModule} from '@angular/http';



describe('LookupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpModule
      ],
      providers: [LookupService]
    });
  });

  it('should be created', inject([LookupService], (service: LookupService) => {
    expect(service).toBeTruthy();
  }));
});
