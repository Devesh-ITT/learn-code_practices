import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { environment } from '../../../environments/environment';
import { AppService } from '../app.service';

@Injectable()
export class LookupService {
  httpOptions: any;

  /**
   * Creates a new KnowledgeBaseContentervice with the injected HttpClient.
   * @param {HttpClient} http - The injected HttpClient.
   * @constructor
   */
  private apiUrl = environment.API_ENDPOINT;
  constructor(
    private http: HttpClient,
    private appService: AppService
  ) {
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': environment.BASE_TOKEN })
    };
  }

  setoAuthLinks(links) {
    localStorage.setItem('authLinks', JSON.stringify(links));
  }

  getoAuthLinks() {
    return JSON.parse(localStorage.getItem('authLinks'));
  }

  setServicesLinks(links) {
    localStorage.setItem('serviceLinks', JSON.stringify(links));
  }

  getServicesLinks() {
    return JSON.parse(localStorage.getItem('serviceLinks'));
  }

  getProviders(hashEmail, isSignUp): Observable<any> {
    // base64 encoding of Md5(email)
    hashEmail = btoa(hashEmail);
    console.log(hashEmail);
    let providerURL = environment.PROVIDERS + '' + hashEmail;
    if (isSignUp) {
      providerURL = environment.DEFAULT_PROVIDERS;
    }

    console.log(providerURL);
    this.httpOptions.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'providerURL': providerURL,
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.get(environment.LOCAL_API_ENDPOINT + '/api/getProvider', this.httpOptions)
      .catch(this.handleErrorObservable);
  }

  getServiceDiscription(serDisUrl): Observable<any> {
    this.httpOptions.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'serdisurl': serDisUrl,
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.get(environment.LOCAL_API_ENDPOINT + '/api/getServiceDiscriptions', this.httpOptions)
      .catch(this.handleErrorObservable);
  }

  loginUsingAzureAD(serURl, auth_code): Observable<any> {
    const body = new HttpParams()
      .set('grant_type', 'authorization_code')
      .set('code', auth_code)
      .set('redirect_uri', this.getRedirectionUrl());
    this.httpOptions.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': this.appService.getAuthorizationCode(),
      'serURl': serURl
    });
    return this.http.post(environment.LOCAL_API_ENDPOINT + '/api/azureadLogin', body.toString(), this.httpOptions)
      .catch(this.handleErrorObservable);
  }

  // returns the Redirect Url; redirect from AzureAd authentication
  public getRedirectionUrl(): string {
    const currentUrl = window.location.href;
    const splittedUrl = currentUrl.split('/#');
    const actualRedirectUrl = splittedUrl[0];
    return encodeURIComponent(actualRedirectUrl);
  }

  // Get call for JOBS-LIST
  public getJobs(jobListApiUrl: string) {
    const accessTokenObj = JSON.parse(localStorage.getItem('userToken'));
    this.httpOptions.headers = new HttpHeaders({
      'Authorization': 'Bearer ' + accessTokenObj.token,
      'Content-Type': 'application/json',
      'joblistapiurl': jobListApiUrl
    });
    console.log(this.httpOptions.headers);
    return this.http.get(environment.LOCAL_API_ENDPOINT + '/api/getJobsList', this.httpOptions)
      .catch(this.handleErrorObservable);
  }
  /**
    * Handle HTTP error
    */
  private handleErrorObservable(error: Response | any) {
    console.error(error.status || error);
    if (error.name.toLowerCase() === 'timeouterror') {
      return Observable.throw({ name: 'requestTimeOut' });
    }
    return Observable.throw(error.status || error);
  }
}
