import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { environment } from '../../../environments/environment';
import { Registration } from '../../modals/registration';
import { Login } from '../../modals/login';

@Injectable()
export class AuthService {
  httpOptions: any;

  /**
   * Creates a new KnowledgeBaseContentervice with the injected HttpClient.
   * @param {HttpClient} http - The injected HttpClient.
   * @constructor
   */
  private apiUrl = environment.API_ENDPOINT;

  constructor(private http: HttpClient) {
      this.httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': environment.BASE_TOKEN })
      };
  }


  /**
   * Returns an Observable for the HTTP POST request for the JSON resource.
   * @return {Login} The Observable for the HTTP request.
   */

  loginUser (serURl, loginInfo): Observable<any> {
      const body = new HttpParams()
      .set('grant_type', loginInfo.grant_type)
      .set('username', loginInfo.email)
      .set('password', loginInfo.password);
      this.httpOptions.headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': environment.BASE_TOKEN,
        'serURl' : serURl
      });
      return this.http.post(environment.LOCAL_API_ENDPOINT + '/api/loginUser', body.toString(), this.httpOptions)
                  .catch(this.handleErrorObservable);
  }

  logoutUser() {
    localStorage.removeItem('userToken');
    localStorage.removeItem('authLinks');
    localStorage.removeItem('serviceLinks');
  }

  /**
   * Returns an Observable for the HTTP POST request for the JSON resource.
   * @return {Registration} The Observable for the HTTP request.
   */

  createUser (userInfo): Observable<any> {
      this.httpOptions.headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': environment.BASE_TOKEN });
      return this.http.post(this.apiUrl + 'scim/Users', userInfo, this.httpOptions)
                  .catch(this.handleErrorObservable);
  }

  /* Reset password */
  resetPassword (resetInfo): Observable<any> {
      this.httpOptions.headers = new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': environment.FORGOT_TOKEN });
      return this.http.post(environment.FORGOT_ENDPOINT, resetInfo, this.httpOptions)
                  .catch(this.handleErrorObservable);
  }

  /**
  * Handle HTTP error
  */
   private handleErrorObservable (error: Response | any) {
    console.error(error.status || error);
    if (error.name.toLowerCase() === 'timeouterror') {
      return Observable.throw({name: 'requestTimeOut'});
    } else {
      return Observable.throw(error.status || error);
    }
  }
}
