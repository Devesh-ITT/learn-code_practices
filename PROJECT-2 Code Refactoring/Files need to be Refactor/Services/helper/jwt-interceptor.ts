import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/timeout';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { MessageService } from '../message/message';
import { AuthService } from '../auth/auth.service';
import { ToasterService } from 'angular2-toaster';



@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(
        private spinnerService: Ng4LoadingSpinnerService,
        private auth: AuthService,
        private router: Router,
        private messageService: MessageService,
        private toasterService: ToasterService
    ) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let requestTimeoutPeriod: number;
        if (request instanceof HttpRequest) {
            this.spinnerService.show();
            requestTimeoutPeriod = Number(request.headers.get('timeout')) || 30000;
        }

        if (!window.navigator.onLine) {
            this.spinnerService.hide();
            this.toasterService.clear();
            this.toasterService.pop('error', '', 'Network Error! Please check your internet connection and refresh your page!');
            return Observable.throw(new HttpErrorResponse({ error: 'Internet is required.' }));
        } else {
            return next.handle(request).do((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    this.spinnerService.hide();
                    console.log(event);
                    if (event.body.error && +event.body.error.status === 401) {
                        this.auth.logoutUser();
                        this.router.navigate(['/login']).then(() =>
                            this.toasterService.pop('error', '', 'Unauthorised access detected, Please login again!')
                        );
                    }
                }
            }, (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    this.spinnerService.hide();
                    this.toasterService.clear();
                    console.log(err);
                    if (err.error === 'Unauthorized access') {
                        this.auth.logoutUser();
                        this.router.navigate(['/login']).then(() =>
                            this.toasterService.pop('error', '', 'Unauthorised access detected, Please login again!')
                        );
                    } else {
                        this.toasterService.pop('error', '', 'Something went wrong!, ' + err.message);
                    }
                }
            }).timeout(requestTimeoutPeriod);
        }
    }
}
