import { Component, OnInit} from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Login } from '../../shared/modals/login';
import { Router, ActivatedRoute } from '@angular/router';
import {ToasterService} from 'angular2-toaster';
import { AuthService } from '../../shared/service/auth/auth.service';
import { LookupService } from '../../shared/service/lookup/lookup.service';
import {Md5} from 'ts-md5/dist/md5';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { environment } from '../../environments/environment.prod';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: FormGroup;
  userEmailTemp: string;
  isNotValidate = true;
  loginModel: Login;
  isUserReadyToLogin = false;
  loginSerUrl = '';
  constructor(private lookupService: LookupService,
    private auth: AuthService,
    private spinnerService: Ng4LoadingSpinnerService,
    private toasterService: ToasterService,
    private fb: FormBuilder,
    private activatedRouter: ActivatedRoute,
    private router: Router) {
      if (localStorage.getItem('userToken')) {
        this.router.navigate(['/showme']);
      }
    }

  ngOnInit() {
    const urlCameBackFromAzureAD = localStorage.getItem('redirectedUrl');
    const serviceDescriptionDataForOAuth2 = JSON.parse(localStorage.getItem('serviceDescriptionData'));
    console.log('serviceDescriptionDataForOAuth2', serviceDescriptionDataForOAuth2);
    localStorage.removeItem('redirectedUrl');
    let AzureAD_Code = '';
    if (urlCameBackFromAzureAD && urlCameBackFromAzureAD.indexOf('?') > 0) {
      const queryData = urlCameBackFromAzureAD.split('?');
      const splittedQueryData = queryData[1].split('&');
      const queryParameters = [];
      splittedQueryData.map(keyValuePair => {
        const splittedKeyAndValue = keyValuePair.split('=');
        queryParameters.push({key: splittedKeyAndValue[0], value: splittedKeyAndValue[1]});
      });
      console.log(queryParameters);
      queryParameters.map(
        params => {
          if (params.key === 'code') {
            AzureAD_Code = params.value;
            const oAuth2Links = JSON.parse(localStorage.getItem('authLinks'));
            let oAuth2TokenApiUrl;
            if (oAuth2Links ) {
              oAuth2TokenApiUrl = oAuth2Links.find(function(element, index) {
                return element.rel === 'token';
              });
            }
            console.log(oAuth2TokenApiUrl);
            if (oAuth2TokenApiUrl && AzureAD_Code) {
              this.lookupService.loginUsingAzureAD(oAuth2TokenApiUrl.href, AzureAD_Code).subscribe(
                (data) => {
                  console.log(data);
                  localStorage.setItem('userToken', JSON.stringify({token : data.access_token}));
                  localStorage.setItem('AuzerAuthTokens', JSON.stringify(data));
                  this.router.navigate(['/showme']);
                },
                (error) => {
                  if (error.name === 'requestTimeOut') {
                    this.spinnerService.hide();
                    this.toasterService.pop('error', '', 'Request Timed out, Please try again!');
                  }
                }
              );
            }
          }
        }
      );
    }

    this.user = this.fb.group({
      password: ['', Validators.required],
      email: [{value: '', disabled: false}, [Validators.required, Validators.email]],
    }, {validator: this.validateConfirmPassword('password')});
  }

  validateConfirmPassword(passwordKey: string) {
    return (group: FormGroup) => {
      const passwordInput = group.controls[passwordKey];
      if (passwordInput.value.length > 0 && passwordInput.value.length < 8 ) {
        return passwordInput.setErrors({notMinimum: true});
      }
    };
  }

  isApplicableForProceedToNext(): boolean {
    return this.user.get('email').invalid;
  }

  isApplicableForProceedToLogin(): boolean {
    return this.user.get('password').invalid;
  }

  chnageNextView() {
    this.isUserReadyToLogin = true;
    this.userEmailTemp =  this.user.get('email').value;
    this.user.get('email').disable();
    this.user.get('password').markAsUntouched();
  }

  cancelAndReEnterEmail() {
    this.isUserReadyToLogin = false;
    this.user.get('email').enable();
    this.userEmailTemp = '';
    this.user.get('password').markAsUntouched();
    this.user.controls['password'].setValue('');
  }

  getLoginModel(item) {
    return {
        email: this.userEmailTemp,
        password: item.password,
        grant_type : 'password'
    };
  }

  onTabForgotPassword() {
    this.getLookupProviders('', false, true);
  }

  onTabCreateAccount() {
    this.getLookupProviders('', true, false);
  }

  onTabNext() {
    this.getLookupProviders(this.user.get('email').value, false, false);
  }

  getServiceDisURL(providers, isSignUp) {
      if (!isSignUp) {
        return providers[0].links[0].href;
      } else {
        const signApiLink = providers.find(function(element, index) {
          return element.defaultProvider === true;
        });

        return signApiLink.links[0].href;
      }
  }
  getLookupProviders(email, isSignUp, isForgot) {
    let popUpWindow;
    if (!email) {
      setTimeout(() => {
        popUpWindow = window.open('', '');
      }, 1000);
    }
    // Checking for lookup services
    const self = this;
    // NzMzZDE3OGQyOWMwNzgwMDc3NmFmNDEwMjdkNjQyMDY=
    // Md5.hashStr(email)
    this.lookupService.getProviders(Md5.hashStr(email), isSignUp)
    .subscribe( data => {
            console.log(data.providers[0].links[0].href);
            const serDisUrl = self.getServiceDisURL(data.providers, isSignUp);
            self.lookupService.getServiceDiscription(serDisUrl)
            .subscribe( sData => {
                    console.log(sData);
                    localStorage.setItem('serviceDescriptionData', JSON.stringify(sData));
                    console.log(sData.access.oauth2.issuer);
                    if (sData.access.oauth2.issuer === 'cps.printeron.com') {
                      const loginApiUrl = sData.access.oauth2.links.find(function(element, index) {
                        return element.rel === 'token';
                      });

                      if (loginApiUrl !== undefined && !isSignUp && !isForgot) {
                        self.loginSerUrl = loginApiUrl.href;
                        self.lookupService.setoAuthLinks(sData.access.oauth2.links);
                        self.lookupService.setServicesLinks(sData.links);
                        self.chnageNextView();
                      } else if (isSignUp) {
                        const signUpUrl = sData.links.find(function(element, index) {
                          return element.rel === 'http://printeron.net/schemas/cloud/print/endpoint/login/signUp';
                        });

                        signUpUrl.href = signUpUrl.href + '/signup?lang=en_US';
                        console.log(signUpUrl);
                        popUpWindow.location.href = signUpUrl.href;

                      } else if (isForgot) {
                        const forgotUrl = sData.links.find(function(element, index) {
                          return element.rel === 'http://printeron.net/schemas/cloud/print/endpoint/login/reset';
                        });

                        forgotUrl.href = forgotUrl.href + '/forgot?lang=en_US&email=' + email;
                        console.log(forgotUrl);
                        popUpWindow.location.href = forgotUrl.href;
                      }
                    } else {
                        const oAuthApiUrl = sData.access.oauth2.links.find(function(element, index) {
                          return element.rel === 'authorization';
                        });
                        console.log(oAuthApiUrl.href);
                        const firstParam = '?response_type=code&scope=openid&client_id=' + environment.CLIENT_ID;
                        const finalParams = firstParam + '&state=AuthenticateUser&redirect_uri=' + this.lookupService.getRedirectionUrl();
                        const oAuthFinalUrl = oAuthApiUrl.href + '' + finalParams;
                        self.lookupService.setoAuthLinks(sData.access.oauth2.links);
                        self.lookupService.setServicesLinks(sData.links);
                        this.redirectToAzureADLogin(oAuthFinalUrl);
                    }
                },
                error => {
                  if (error.name === 'requestTimeOut') {
                    this.spinnerService.hide();
                    this.toasterService.pop('error', '', 'Request Timed out, Please try again!');
                  }
                }
            );
         },
        error => {
          if (error.name === 'requestTimeOut') {
            this.spinnerService.hide();
            this.toasterService.pop('error', '', 'Request Timed out, Please try again!');
          }
        }
    );
  }

  redirectToAzureADLogin(redirectionUrl) {
    window.location.href = redirectionUrl;
  }

  onTabLogin({value, valid}: {value: any, valid: boolean }) {
    this.loginModel = this.getLoginModel(value);
    console.log(this.loginModel);
    this.auth.loginUser(this.loginSerUrl, this.loginModel)
    .subscribe( data => {
            if (data && data.access_token !== null && data.access_token !== undefined) {
              localStorage.setItem('userToken', JSON.stringify({token : data.access_token}));
              this.router.navigateByUrl('/showme');
            } else {
              this.toasterService.pop('error', '', 'Incorrect Email or Password');
            }
         },
        error => {
            console.log(error);
            if (error.name === 'requestTimeOut') {
              this.spinnerService.hide();
              this.toasterService.pop('error', '', 'Request Timed out, Please try again!');
            } else {
              this.toasterService.pop('error', '', 'Something went wrong, Please try again!');
            }
        }
    );
  }

  openURLInNewTab(url) {
    console.log(url);
    window.open(url, '_blank');
  }
}
