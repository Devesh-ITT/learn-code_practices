import java.util.*;
import java.io.*;

class UserProfile {
    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
    int numberOfFriends, numberOfFriendsToBeDelete;
    LinkedList<Integer> friendsList = new LinkedList<Integer>();
    boolean isUserInputValid = true;
    final int MIN_NUMBER_OF_FRIENDS = 1;
    final int MAX_NUMBER_OF_FRIENDS = 100000;
    final int MIN_TEST_CASES = 1;
    final int MAX_TEST_CASES = 1000;
    
    
    public void friendsRemovalBasedOnPopularity() throws Exception {
        getInputForTotalFriendsAndNumberOfFriendsToDelete();
        if(!isValidInput()){
            isUserInputValid = false;
            return;
        }
        String[] friendsPopularityArray = getFriendsPopularity();
        removeFriends(friendsPopularityArray);
        displayRemainingFriends();
        friendsList.clear();
    }
    
    public void getInputForTotalFriendsAndNumberOfFriendsToDelete() throws Exception {
        String inputStreamFor_numberOfFriends_numberOfFriendsToDelete = bufferReader.readLine();
        numberOfFriends = Integer.parseInt(inputStreamFor_numberOfFriends_numberOfFriendsToDelete.split(" ")[0]);
        numberOfFriendsToBeDelete = Integer.parseInt(inputStreamFor_numberOfFriends_numberOfFriendsToDelete.split(" ")[1]);
    }
    
    public boolean isValidInput() {
        if(numberOfFriends < 1 || numberOfFriends > MAX_NUMBER_OF_FRIENDS){
            return false;
        }
        if(numberOfFriendsToBeDelete < 0 || numberOfFriendsToBeDelete > numberOfFriends){
            return false;
        }
        return true;
    }
    
    public String[] getFriendsPopularity() throws Exception {
        String[] friendsPopularityArray = bufferReader.readLine().split(" ");
        return friendsPopularityArray;
    }
    
    public void removeFriends(String[] friendsPopularityArray) {
        for(int friendCount = 0 ; friendCount < numberOfFriends ; friendCount++){
            int popularityOfFriend = Integer.parseInt(friendsPopularityArray[friendCount]);
            while(!friendsList.isEmpty() && numberOfFriendsToBeDelete != 0 && friendsList.getLast() < popularityOfFriend){              
                friendsList.removeLast();
                numberOfFriendsToBeDelete--;
            }
            friendsList.addLast(popularityOfFriend);
        }
        while(numberOfFriendsToBeDelete != 0){
            friendsList.removeLast();
            numberOfFriendsToBeDelete--;
        }
    }
    
    public void displayRemainingFriends() {
        while(!friendsList.isEmpty()){
            System.out.print(friendsList.removeFirst()+" ");
        }
        System.out.println();
    }
    
    public static void main(String args[] ) throws Exception {
        UserProfile userProfile = new UserProfile();
        int testCases = Integer.parseInt(userProfile.bufferReader.readLine());
        if(testCases < userProfile.MIN_TEST_CASES || testCases > userProfile.MAX_TEST_CASES ){
            return;
        }
        while(testCases != 0){
            userProfile.friendsRemovalBasedOnPopularity();
            testCases--;
        }
    }
}
