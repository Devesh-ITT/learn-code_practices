import org.junit.tests.AllTestsTest.JUnit4Test;
import static org.junit.Assert.*;
import org.junit.Test;
import junit.*;

public class MatrixTestCases extends JUnit4Test{
	
	@Test
	public void testForNumberOfInputForDefaultRowsCols() throws Exception {
		//Arrange
		Matrix matrix = new Matrix();
		
		//Act
		int actualNumberOfInputs = matrix.getTotalNumnerOfData();
		
		//Assert
		assertEquals(9, actualNumberOfInputs);
	}
	
	@Test
	public void testForNumberOfInputForCustomizedRowsCols() throws Exception{
		//Arrange
		Matrix matrix = new Matrix(4,3);
		
		//Act
		int actualNumberOfInputs = matrix.getTotalNumnerOfData();
		int expectedNumberOfInputs = 4 * 3;
		
		//Assert
		assertEquals(expectedNumberOfInputs, actualNumberOfInputs);
	}
	
	@Test
	public void getDataAtSpecificLocation_dataAtLocation() throws Exception{
		//Arrange
		Matrix matrix = new Matrix(2,3);
		int[][] dummyArray = {{1,2,3},{4,5,6}};
		
		//Act
		matrix.setMatrix(dummyArray);
		int elementAtLocation0x0 = matrix.getDataAtSpecificLocation(0, 0);
		int elementAtLocation0x1 = matrix.getDataAtSpecificLocation(0, 1);
		int elementAtLocation0x2 = matrix.getDataAtSpecificLocation(0, 2);
		int elementAtLocation1x0 = matrix.getDataAtSpecificLocation(1, 0);
		int elementAtLocation1x1 = matrix.getDataAtSpecificLocation(1, 1);
		int elementAtLocation1x2 = matrix.getDataAtSpecificLocation(1, 2);
		
		//Assert
		assertEquals(1, elementAtLocation0x0);
		assertEquals(2, elementAtLocation0x1);
		assertEquals(3, elementAtLocation0x2);
		assertEquals(4, elementAtLocation1x0);
		assertEquals(5, elementAtLocation1x1);
		assertEquals(6, elementAtLocation1x2);
	}
	
	@Test
	public void testForSumOfElementsAtEvenIndex() throws Exception{
		//Arrange
		Matrix matrix = new Matrix();
		int[][] dummyArray = {{1,2,3},{4,5,6},{7,8,9}};
		
		//Act
		matrix.setMatrix(dummyArray);
		int sumOfAllElementsAtEvenLocations = matrix.sumOfElementsAtEvenIndex();
		
		//Assert
		assertEquals(25, sumOfAllElementsAtEvenLocations);
	}
	
	@Test
	public void testForSumOfElementsAtOddIndex() throws Exception{
		//Arrange
		Matrix matrix = new Matrix();
		int[][] dummyArray = {{1,2,3},{4,5,6},{7,8,9}};
		
		//Act
		matrix.setMatrix(dummyArray);
		int sumOfAllElementsAtOddLocations = matrix.sumOfElementsAtOddIndex();
		
		//Assert
		assertEquals(20, sumOfAllElementsAtOddLocations);
	}
}
