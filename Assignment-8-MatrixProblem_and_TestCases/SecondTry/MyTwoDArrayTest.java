import org.junit.tests.AllTestsTest.JUnit4Test;
import static org.junit.Assert.*;
import org.junit.Test;
import junit.*;

public class MyTwoDArrayTest extends JUnit4Test{
	
	@Test
	public void testForScannerReaderInstance() throws Exception {
		//Arrange
		FactroyForInputReader factoryForInputReader = new FactroyForInputReader();
		InputReader instanceOfInputReader;
		
		//Act
		instanceOfInputReader = factoryForInputReader.getReaderInstance("BUFFER_READER");
		
		//Assert
		//assertEquals();
	}
	
	@Test
	public void testInputForTestCases() throws Exception{
		//Arrange
		MyTwoDArrayInput myTwoDArrayInput = new MyTwoDArrayInput();
		
		//Act
		
		//Assert
	}
	
	@Test
	public void getDataAtSpecificLocation_dataAtLocation() throws Exception{
		//Arrange
		TwoDArray twoDArray = new TwoDArray(2,3);
		int[] dummyInputArray = {1,2,3,4,5,6};
		
		//Act
		twoDArray.setTwoDArray(dummyInputArray);
		int elementAtLocation0x0 = twoDArray.getElementOfTwoDArray(0, 0);
		int elementAtLocation0x1 = twoDArray.getElementOfTwoDArray(0, 1);
		int elementAtLocation0x2 = twoDArray.getElementOfTwoDArray(0, 2);
		int elementAtLocation1x0 = twoDArray.getElementOfTwoDArray(1, 0);
		int elementAtLocation1x1 = twoDArray.getElementOfTwoDArray(1, 1);
		int elementAtLocation1x2 = twoDArray.getElementOfTwoDArray(1, 2);
		
		//Assert
		assertEquals(1, elementAtLocation0x0);
		assertEquals(2, elementAtLocation0x1);
		assertEquals(3, elementAtLocation0x2);
		assertEquals(4, elementAtLocation1x0);
		assertEquals(5, elementAtLocation1x1);
		assertEquals(6, elementAtLocation1x2);
	}
	
	@Test
	public void testForAdditionOfAlternatePositions() throws Exception{
		//Arrange
		TwoDArray twoDArray = new TwoDArray();
		int[] dummyInputArray = {1,2,3,4,5,6,7,8,9};
		
		//Act
		twoDArray.setTwoDArray(dummyInputArray);
		int sumOfAlternativesStartingFrom0x0 = twoDArray.getSumOfAlternatePositions(0,0);
		int sumOfAlternativesStartingFrom0x1 = twoDArray.getSumOfAlternatePositions(0,1);
		int sumOfAlternativesStartingFrom2x0 = twoDArray.getSumOfAlternatePositions(2,0);
		int sumOfAlternativesStartingFrom3x3 = twoDArray.getSumOfAlternatePositions(3,3);
		
		//Assert
		assertEquals(25, sumOfAlternativesStartingFrom0x0);
		assertEquals(20, sumOfAlternativesStartingFrom0x1);
		assertEquals(18, sumOfAlternativesStartingFrom2x0);
		assertEquals(9, sumOfAlternativesStartingFrom3x3);
	}
}
