import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

abstract class InputReader{
	public abstract int[] read(int numberOfDataTobeRead) throws Exception;
} 

class InputDataReaderFromScanner extends InputReader{
	Scanner scanner = new Scanner(System.in);
	public int[] read(int numberOfDataTobeRead) throws Exception{
		throw new Exception("Not Implemented");
	}
}

class InputDataReaderFromBufferedReader extends InputReader{
	BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	public int[] read(int numberOfDataTobeRead)throws Exception{
		throw new Exception("Not Implemented");
	}
}

class FactroyForInputReader{
	public InputReader getReaderInstance(String typeOfReader) throws Exception{
		throw new Exception("Not yet implemented");
	}
}

class MyTwoDArrayInput{
	InputReader instanceOfInputReader;
	private void setInstanceOfInputReader(String typeOfReader){}
	private InputReader getInstanceOfInputReader(){
		return null;
	}
	public int getTotalNumberOfTestCases(){
		return 0;
	}
	public int[] getPositionToStartAlternateAddition(){
		return null;
	} 
	public int[] getUserInputs(int numberOfInputs){
		return null;
	}
}

interface iTwoDArray {
	public void setTwoDArray(int[] inputForTwoDArray);
	public int[][] getTwoDArray() throws Exception;
	public int getElementOfTwoDArray(int X, int Y) throws Exception;
	public int getSumOfAlternatePositions(int startingX, int startingY) throws Exception;
}

class TwoDArray implements iTwoDArray {
	private int[][] twoDArray;
	private int totalRows;
	private int totalColums;
	// default rows and columns are 3
	TwoDArray(){}
	TwoDArray(int totalRows, int totalColumns){}
	public void setTwoDArray(int[] inputForTwoDArray){
		
	}
	public int[][] getTwoDArray() throws Exception{
		throw new Exception("Not yet IMplemented");
	}
	public int getSumOfAlternatePositions(int startingX, int startingY) throws Exception{
		throw new Exception("Not yet Implemented");
	}
	@Override
	public int getElementOfTwoDArray(int X, int Y) throws Exception {
		return 0;
	}
}

class MyTwoDArray{
	public void displayOnConsole(){

	}
	public void startOperations(){

	}
	public static void main(String[] args){

	}
}