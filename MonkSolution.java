import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ArrayList;*


public class MonkSolution {
  private BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
  private Queue<Spider> spiderQueue = new LinkedList<Spider>();
  private int numberOfSpiders;
  private int numberOfIterationsOverSpiders;
  private boolean isUserInputOk_flag = true;
  private final int MIN_ITERATIONS = 1;
  private final int MAX_ITERATIONS = 316;

  public static void main(String[] args) throws Exception{
    MonkSolution rescueProcess = new MonkSolution();
    rescueProcess.startProcess();
    if(!rescueProcess.isUserInputOk_flag){
      return;
    }
  }

  public void startProcess() throws Exception{
    String spiderNumber_iterationCount_inputString = bufferReader.readLine();
    if(isUserInputNotValid(spiderNumber_iterationCount_inputString)){
      isUserInputOk_flag = false;
      return;
    }
    int arrayOfSpidersPower[] = getPowerOfSpidersToBeInsertInQueue();
    addSpidersWithPowerIntoSpiderQueue(arrayOfSpidersPower);
    for (int iterationCount = 0 ; iterationCount < numberOfIterationsOverSpiders ; iterationCount++) {
      int numberOfSpiderForDequeue = getNumberOfSpidersForDequeue();
      Spider[] dequeuedSpiders = getDequeuedSpiders(numberOfSpiderForDequeue);
      Spider spiderWithMaxPower = getSpiderHavingMaximumPower(dequeuedSpiders);
      System.out.print(spiderWithMaxPower.getPosition()+" ");
      // spiders for enqueue = [ dequeued Spiders, with decreased power ] - spider having max power
      ArrayList<Spider> spidersForEnqueue = getSpidersForEnqueueBack(dequeuedSpiders, spiderWithMaxPower);
      enqueueSpiderBackToTheQueue(spidersForEnqueue);
    }
  }

  public boolean isUserInputNotValid(String inputStream){
    String[] inputStreamArray = inputStream.split(" ");
    numberOfSpiders = Integer.parseInt(inputStreamArray[0]);
    numberOfIterationsOverSpiders = Integer.parseInt(inputStreamArray[1]);
    if(numberOfIterationsOverSpiders < MIN_ITERATIONS || numberOfIterationsOverSpiders > MAX_ITERATIONS){
      return true;
    }
    int minLimitOfSpiders = numberOfIterationsOverSpiders;
    int maxLimitOfSpiders = minLimitOfSpiders * minLimitOfSpiders;
    if(numberOfSpiders < minLimitOfSpiders || numberOfSpiders > maxLimitOfSpiders){
      return true;
    }
    return false;
  }

  public int[] getPowerOfSpidersToBeInsertInQueue() throws Exception{
    String inputStringFor_powerOfSpiders[] = bufferReader.readLine().split(" ");
    int powerOfSpiders[] = new int[ numberOfSpiders ];
    for(int indexOfSpidersPower = 0 ; indexOfSpidersPower < numberOfSpiders ; indexOfSpidersPower++ ){
      powerOfSpiders[indexOfSpidersPower] = Integer.parseInt(inputStringFor_powerOfSpiders[indexOfSpidersPower]);
    }
    return powerOfSpiders;
  }

  public void addSpidersWithPowerIntoSpiderQueue( int[] spidersPowerArray){
    int positionOfSpider = 1;
    for(int spiderPower : spidersPowerArray){
      Spider spider = new Spider(positionOfSpider, spiderPower);
      spiderQueue.add(spider);
      positionOfSpider++;
    }
  }

  public int getNumberOfSpidersForDequeue(){
    return (numberOfIterationsOverSpiders < spiderQueue.size()) ? numberOfIterationsOverSpiders : spiderQueue.size();
  }

  public Spider[] getDequeuedSpiders(int numberOfSpidersForDequeue){
    Spider[] tempArrayToHoldDequeuedSpiders = new Spider[ numberOfSpidersForDequeue ];
    for (int indexValueForTempArray = 0; indexValueForTempArray < numberOfSpidersForDequeue; indexValueForTempArray++) {
      tempArrayToHoldDequeuedSpiders[indexValueForTempArray] = spiderQueue.remove();
    }
    return tempArrayToHoldDequeuedSpiders;
  }

  public Spider getSpiderHavingMaximumPower(Spider[] dequeuedSpiders){
    int maxPowerAmongSpiders = dequeuedSpiders[0].getPower();
    Spider spiderHavingMaxPower = dequeuedSpiders[0];
    for (Spider spider :  dequeuedSpiders) {
      if(spider.getPower() > maxPowerAmongSpiders){
        maxPowerAmongSpiders = spider.getPower();
        spiderHavingMaxPower = spider;
      }
    }
    return spiderHavingMaxPower;
  }

  public ArrayList<Spider> getSpidersForEnqueueBack(Spider[] dequeuedSpider, Spider spiderToBeExclude){
    ArrayList<Spider> spidersForEnqueue = new ArrayList<Spider>();
    for(Spider spider : dequeuedSpider) {
      if(spider != spiderToBeExclude){
        spider.decreasePowerByOne();
        spidersForEnqueue.add(spider);
      }
    }
    return spidersForEnqueue;
  }

  public void enqueueSpiderBackToTheQueue(ArrayList<Spider> spidersForEnqueue){
    for(Spider spider : spidersForEnqueue){
      spiderQueue.add(spider);
    }
  }

}

class Spider{
  private int position;
  private int power;

  public Spider(int position, int power){
    this.position = position;
    this.power = power;
  }

  public int getPosition(){
    return position;
  }

  public int getPower(){
    return power;
  }

  public void decreasePowerByOne(){
    if(power != 0){
      power--;
    }
  }
}
