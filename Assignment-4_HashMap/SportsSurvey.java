import java.util.*;
import java.io.*;

public class SportsSurvey {
    HashMap<String, Integer> detailsOfSportsSurvey = new HashMap<String, Integer>();
    CustomisedInputClass inputAPI;
    boolean isUserInputValid = true;

    SportsSurvey(){
        inputAPI = new CustomisedInputClass(System.in);
        int footballLover = 0;
    }
    
    void startSurvey(int numberOfPeopleForSurvey ){
        for(int counter = 0 ; counter < numberOfPeopleForSurvey ; counter++){
            String nameOfPerson = inputAPI.readString();
            String favSport = inputAPI.readString();
            if(isValidInput(nameOfPerson, favSport)){
                maintainSurveyRecords(favSport);
            }else{
                isUserInputValid = false;
                return;
            }
        }
    }
    
    public boolean isValidInput(String nameOfPerson, String favSport){
        return (nameOfPerson.length() <= 10 && favSport.length() <= 10);
    }

    public void maintainSurveyRecords(String sportsName){
        if(detailsOfSportsSurvey.containsKey(sportsName)){
            int totalNumberOfPlayers =  detailsOfSportsSurvey.get(sportsName);
            detailsOfSportsSurvey.put(sportsName, ++totalNumberOfPlayers);
        }else{
            detailsOfSportsSurvey.put(sportsName, 1);
        }
    }
    
    public String mostLovedSportsAmongPeople(){
        String mostLovedSport = null;
        int numberOfPeople = 0;
        for(Map.Entry<String, Integer> entryOfSurvey : detailsOfSportsSurvey.entrySet()){
            if(entryOfSurvey.getValue() > numberOfPeople){
                mostLovedSport = entryOfSurvey.getKey();  
                numberOfPeople = entryOfSurvey.getValue();
            }
        }
        return mostLovedSport;
    }
    
    public int getNumberOfPeopleWhoLikePerticularSports(String sportsName){
        if(detailsOfSportsSurvey.containsKey(sportsName)){
            return detailsOfSportsSurvey.get(sportsName);
        }else{
            return 0;
        }
    }

    public static void main(String args[] ) throws Exception {
        SportsSurvey surveyProcess = new SportsSurvey();
        int numberOfPeopleToBeSurvey = surveyProcess.inputAPI.readInt();
        if(numberOfPeopleToBeSurvey < 1 || numberOfPeopleToBeSurvey > 100000){
            return;
        }
        surveyProcess.startSurvey(numberOfPeopleToBeSurvey);
        if(!surveyProcess.isUserInputValid){
            return;
        }
        System.out.println(surveyProcess.mostLovedSportsAmongPeople());
        System.out.println(surveyProcess.getNumberOfPeopleWhoLikePerticularSports("football"));
    }
}


class CustomisedInputClass {
 
    private InputStream stream;
    private byte[] buf = new byte[1024];
    private int curChar;
    private int numChars;
    private SpaceCharFilter filter;
 
    public CustomisedInputClass(InputStream stream) {
        this.stream = stream;
    }
 
    public int read() {
        if (numChars == -1)
            throw new InputMismatchException();
        if (curChar >= numChars) {
            curChar = 0;
            try {
                numChars = stream.read(buf);
            } catch (IOException e) {
                throw new InputMismatchException();
            }
            if (numChars <= 0)
                return -1;
        }
        return buf[curChar++];
    }
 
    public int readInt() {
        int c = read();
        while (isSpaceChar(c))
            c = read();
        int sgn = 1;
        if (c == '-') {
            sgn = -1;
            c = read();
        }
        int res = 0;
        do {
            if (c < '0' || c > '9')
                throw new InputMismatchException();
            res *= 10;
            res += c - '0';
            c = read();
        } while (!isSpaceChar(c));
        return res * sgn;
    }
 

    public String readString() {
        int c = read();
        while (isSpaceChar(c))
            c = read();
        StringBuilder res = new StringBuilder();
        do {
            res.appendCodePoint(c);
            c = read();
        } while (!isSpaceChar(c));
        return res.toString();
    }
 
    public boolean isSpaceChar(int c) {
        if (filter != null)
            return filter.isSpaceChar(c);
        return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
    }
 
    public interface SpaceCharFilter {
        boolean isSpaceChar(int ch);
    }
    
}