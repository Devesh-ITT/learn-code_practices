import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
public class TestClassClean {
	int newPlayer, previousPlayer;
	int output[];
	boolean isallConstraintsOk = true;
	final String PASS_TO_ANOTHER ="p";
	final String PASS_BACK="b";
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
	public void gameStart(int testCases) throws Exception{
	    int totalPasses,playerID;
		output = new int[testCases];
		for(int a =0 ; a<testCases ; a++){
			String noOfpassesAndInitialPlayerId[] = (br.readLine()).split(" ");
			totalPasses = Integer.parseInt(noOfpassesAndInitialPlayerId[0]);
			
			if(!isPassNumberValid(totalPasses)){
				isallConstraintsOk = false;
				return;
			}
			
			playerID = Integer.parseInt(noOfpassesAndInitialPlayerId[1]);
			
			if(!isValidPlayerID(playerID)){
				isallConstraintsOk = false;
				return;
			}
			
			newPlayer = playerID;
			for(int b = 0 ; b<totalPasses ; b++){
				passesStart();
				
				if(!isallConstraintsOk){
					return;
				}
			}
			output[a] = newPlayer;
			newPlayer =0;
			previousPlayer =0;
		}
	}
	
	public void passesStart() throws Exception{
		String[] passInput = (br.readLine()).split(" ");
		if(passInput[0].equalsIgnoreCase(PASS_TO_ANOTHER)){
			int idToPass = Integer.parseInt(passInput[1]);
			
			if(!isValidPlayerID(idToPass)){
				isallConstraintsOk = false;
				return;
			}
			
			passToAnother(idToPass);
		}
		if(passInput[0].equalsIgnoreCase(PASS_BACK)){
			passBack();
		}
	}
	public void passToAnother(int playerID){
		previousPlayer = newPlayer;
		newPlayer = playerID;
	}
	
	public void passBack(){
		newPlayer = newPlayer + previousPlayer;
		previousPlayer = newPlayer - previousPlayer;
		newPlayer = newPlayer - previousPlayer;
	}
	
	public void displayOutput(){
		for(int index=0;index<output.length;index++){
			System.out.println("Player "+output[index]);
		}
	}
	
	public boolean isPassNumberValid(int numberOfPasses){
		if(numberOfPasses>=1 && numberOfPasses <=100000){
			return true;
		}else{
			return false;
		}
	}
	public boolean isTestCasesNumberValid(int numberOfTestCases){
		if(numberOfTestCases>=1 && numberOfTestCases <=100){
			return true;
		}else{
			return false;
		}
	}
	public boolean isValidPlayerID(int playerId){
		if(playerId>=1 && playerId <=1000000){
			return true;
		}else{
			return false;
		}
	}
	
	public static void main(String[] args) throws java.lang.Exception {
		TestClassClean footballProblem = new TestClassClean();
		int testCases = Integer.parseInt(footballProblem.br.readLine());
		
		if(!footballProblem.isTestCasesNumberValid(testCases)){
			//footballProblem.isallConstraintsOk = false;
			return;
		}
		
		footballProblem.gameStart(testCases);
		
		if(!footballProblem.isallConstraintsOk){
			return;
		}
		footballProblem.displayOutput();
		
	}
}