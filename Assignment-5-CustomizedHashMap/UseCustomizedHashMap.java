package LearnAndCode;

public class UseCustomizedHashMap<K,V> {
	HashMapAPI<K, V> hashMapAPI;
	
	public UseCustomizedHashMap() {
		hashMapAPI = new HashMapAPI<K, V>();
	}
	
	public void addKeyValuePair(K key, V value){
		hashMapAPI.put(hashMapAPI.getHashNode(key, value));
	}
	
	public void printKeyValuePair(){
		hashMapAPI.printHashMap();
	}
	
	public void printHashBucket(){
		hashMapAPI.printHashBucket();
	}
	
	public void containsKey(K key){
		if(hashMapAPI.containsKey(key)){
			System.out.println("'"+key+"' Found!!");
		}else{
			System.out.println("'"+key+"' Not Found!!");
		}
	}
	
	public void remove(K key){
		if(hashMapAPI.remove(key)){
			System.out.println("'"+key+"' Deleted Succeffully!!");
		}else{
			System.out.println("'"+key+"' Something went wrong with deletion!!");
		}
	}
	
	public static void main(String[] args) {
		UseCustomizedHashMap<String, String> myHashMap = new UseCustomizedHashMap<String, String>();
		myHashMap.addKeyValuePair("A", "A's data");
		myHashMap.addKeyValuePair("B", "B's data");
		myHashMap.addKeyValuePair("C", "C's data");
		myHashMap.addKeyValuePair("DE", "DE's data");
		myHashMap.addKeyValuePair("ED", "ED's data");
		myHashMap.addKeyValuePair("CD", "CD's data");
		myHashMap.addKeyValuePair("DC", "DC's data");
		myHashMap.printKeyValuePair();
		myHashMap.printHashBucket();
		myHashMap.containsKey("Ed");
		myHashMap.containsKey("CD");
		myHashMap.containsKey("DE");
		myHashMap.remove("CD");
		myHashMap.printKeyValuePair();
		myHashMap.printHashBucket();
		myHashMap.remove("DE");
		myHashMap.printHashBucket();
		myHashMap.containsKey("devesh");
		
	}

}
