package LearnAndCode;
import java.util.*;

class HashNode<K, V>{
	private K key;
	private V value;
	private int hashValue;
	
	HashNode(K key, V value){
		this.key = key;
		this.value = value;
		this.hashValue = key.hashCode();
	}
	
	public K getKey() {
		return key;
	}
	
	public int getHashCodeOfKey(){
		return hashValue;
	}
	
	public V getValue(){
		return value;
	}
}

 class HashMapAPI<K, V> {
	private int hashBucketSize;
	private ArrayList<LinkedList<HashNode<K,V>>> hashBucket;
	
	HashMapAPI(){
		hashBucketSize = 16;
		hashBucket = new ArrayList<LinkedList<HashNode<K,V>>>(hashBucketSize);
		for(int index=0 ; index<getHashBucketSize() ; index++){
			hashBucket.add(index, null);
		}
	}
	
	HashMapAPI(int defaultSize){
		hashBucketSize = defaultSize;
		hashBucket = new ArrayList<LinkedList<HashNode<K,V>>>(hashBucketSize);
		for(int index = 0 ; index < getHashBucketSize() ; index++){
			hashBucket.add(index, null);
		}
	}
	
	public int getHashBucketSize(){
		return hashBucketSize;
	}
	
	public int getIndexOfHashBucketForHashNode(K key){
		return ( key.hashCode() & (getHashBucketSize() - 1) );
	}
	
	public HashNode<K,V> getHashNode(K key, V value){
		return new HashNode<K,V>(key,value);
	}
	
	public void put(HashNode<K, V> hashNode){
		int index = getIndexOfHashBucketForHashNode(hashNode.getKey());
		if(hashBucket.get(index) == null){
			putWhenBucketIndexIsEmpty(index, hashNode);
		}else{
			putWhenBucketIndexContainsListOfHashNode(index, hashNode);
		}
	}
	
	public void putWhenBucketIndexIsEmpty(int index, HashNode<K,V> hashNode) {
		LinkedList<HashNode<K,V>> listOfHashNode = new LinkedList<HashNode<K,V>>();
		listOfHashNode.add(hashNode);
		hashBucket.remove(index);
		hashBucket.add(index, listOfHashNode);
	}
	
	public void putWhenBucketIndexContainsListOfHashNode(int index, HashNode<K,V> hashNode) {
		LinkedList<HashNode<K,V>> listForHashMap = hashBucket.get(index);
		if(!containsKey(hashNode.getKey())){
			listForHashMap.add(hashNode);
		}
	}
	
	public boolean containsKey(K key){
		int hashCode = key.hashCode();
		int indexAtHashBucket = getIndexOfHashBucketForHashNode(key);
		LinkedList<HashNode<K, V>> listOfHashNodeAtRequiredIndex = hashBucket.get(indexAtHashBucket);
		if(listOfHashNodeAtRequiredIndex != null){
			for(HashNode<K,V> hashNode : listOfHashNodeAtRequiredIndex){
				if(hashNode.getHashCodeOfKey() == hashCode &&  hashNode.getKey().equals(key)){
					return true;
				}
			}
		}
		return false;
	}
	
	public V getValueAssociatedWithKey(K key){
		int hashCode = key.hashCode();
		int indexAtHashBucket = getIndexOfHashBucketForHashNode(key);
		LinkedList<HashNode<K, V>> listOfHashNodeAtRequiredIndex = hashBucket.get(indexAtHashBucket);
		if(listOfHashNodeAtRequiredIndex != null){
			for(HashNode<K,V> hashNode : listOfHashNodeAtRequiredIndex){
				if(hashNode.getHashCodeOfKey() == hashCode &&  hashNode.getKey().equals(key)){
					return hashNode.getValue();
				}
			}
		}
		return null;
	}
	
	public boolean remove(K key){
		int hashCode = key.hashCode();
		int indexAtHashBucket = getIndexOfHashBucketForHashNode(key);
		LinkedList<HashNode<K, V>> listOfHashNodeAtRequiredIndex = hashBucket.get(indexAtHashBucket);
		if(listOfHashNodeAtRequiredIndex != null){
			if(listOfHashNodeAtRequiredIndex.size() == 1){
				return removeWhenOneHashNodeInListOfHashBucket(key,hashCode,listOfHashNodeAtRequiredIndex);
			}else{
				return removeWhenMultipleHashNodeInListOfHashBucket(key,hashCode,listOfHashNodeAtRequiredIndex);
			}
		}
		return false;
	}
	
	public boolean removeWhenOneHashNodeInListOfHashBucket(K key, int hashCode, LinkedList<HashNode<K, V>> linkedListInHashBucket ){
		HashNode<K,V> hashNode = linkedListInHashBucket.getFirst();
		if(hashNode.getHashCodeOfKey() == hashCode && hashNode.getKey().equals(key)){
			int indexOfHashBucket = getIndexOfHashBucketForHashNode(key); // could pass it as 4th argument in this function, would that be ok?
			hashBucket.remove(indexOfHashBucket);
			hashBucket.add(indexOfHashBucket, null);
			return true;
		}
		return false;
	}
	
	public boolean removeWhenMultipleHashNodeInListOfHashBucket(K key, int hashCode, LinkedList<HashNode<K, V>> linkedListInHashBucket){
		int index = 0;
		for(HashNode<K,V> hashNode : linkedListInHashBucket){
			if(hashNode.getHashCodeOfKey() ==  hashCode && hashNode.getKey().equals(key)){
				linkedListInHashBucket.remove(index);
				return true;
			}
			index++;
		}
		return false;
	}
	
	public void printHashMap() {
		for(LinkedList<HashNode<K, V>> hashNodeList : hashBucket){
			if(hashNodeList != null){
				for(int index = 0 ; index < hashNodeList.size(); index++){
					System.out.println(hashNodeList.get(index).getKey()+" --> "+hashNodeList.get(index).getValue());
				}
			}
		}
	}
	
	public void printHashBucket(){
		int index = 0;
		for(LinkedList<HashNode<K, V>> hashNode : hashBucket){
			if(hashNode != null){
				System.out.println(index+" -> "+hashNode);
			}
			index++;
		}
	}
}
  

